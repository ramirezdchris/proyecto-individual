-- DROP DATABASE AgendaPersonal;
CREATE DATABASE AgendaPersonal;
USE AgendaPersonal;

CREATE TABLE tipousuario
(
	id_tipousuario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    tipo VARCHAR(50)
);

CREATE TABLE usuario
(
	id_usuario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(30) NOT NULL,
    apellido VARCHAR(30) NOT NULL,
    email VARCHAR(50) NOT NULL,
    pass VARCHAR(50) NOT NULL,
    subornidado INT,
    id_tipousuario INT NOT NULL,
    constraint fk_usuario_jefe foreign key (subornidado) references usuario(id_usuario) on update cascade
);

CREATE TABLE prioridad
(
	id_prioridad INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    prioridad VARCHAR(9) NOT NULL
);

CREATE TABLE evento
(
	id_evento INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    titulo VARCHAR(25) NOT NULL,
    descripcion VARCHAR(200) NOT NULL,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    hora_inicio TIME NOT NULL, 
    hora_fin TIME NOT NULL, 
    estado VARCHAR(9) NOT NULL,
	id_prioridad INT NOT NULL,
    id_usuario INT NOT NULL
    
);


CREATE TABLE nota
(
	id_nota INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nota VARCHAR(100) NOT NULL,    
	id_evento INT NOT NULL
);

CREATE TABLE rendimiento
(
	id_redimiento INT NOT NULL PRIMARY KEY,
    tareasRealizadas INT NOT NULL,
    calculo DOUBLE NOT NULL,
    id_usuario INT NOT NULL
);

CREATE TABLE asignaciones
(
	id_usuario INT NOT NULL,
    id_evento INT NOT NULL,
    estado VARCHAR(9)
);
	

ALTER TABLE evento ADD CONSTRAINT fk_evento_prioridad FOREIGN KEY(id_prioridad) REFERENCES prioridad(id_prioridad);
ALTER TABLE evento ADD CONSTRAINT fk_evento_usuario FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario);

ALTER TABLE nota ADD CONSTRAINT fk_nota_evento FOREIGN KEY(id_evento) REFERENCES evento(id_evento);

ALTER TABLE rendimiento ADD CONSTRAINT fk_rendimiento_usuario FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario);

ALTER TABLE usuario ADD CONSTRAINT fk_usuario_tipousuario FOREIGN KEY(id_tipousuario) REFERENCES tipousuario(id_tipousuario);

ALTER TABLE asignaciones ADD CONSTRAINT fk_asignaciones_usuarios FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario);
ALTER TABLE asignaciones ADD CONSTRAINT fk_asignaciones_evento FOREIGN KEY(id_evento) REFERENCES evento(id_evento);

/*SELECT * FROM Usuario;
SELECT * FROM evento;

SELECT * FROM prioridad;

SET FOREIGN_KEY_CHECKS = 0;
SET FOREIGN_KEY_CHECKS = 1;
TRUNCATE usuario;

TRUNCATE evento;
*/

INSERT INTO prioridad VALUES(0,'Alta'),(0,'Media'),(0,'Baja');
INSERT INTO tipousuario VALUES(0,'Gerente'),(0,'Recursos Humanos'),(0,'Supervisor'),(0,'Colaborador');

-- SELECT * FROM usuario;

INSERT INTO usuario VALUES(0,'Amanda Beatriz', 'Pineda Vaquerano','amanda@gmail.com','123',null,1);


INSERT INTO usuario VALUES(0,'Kevin Santiago', 'Paz Vasquez','kevin@gmail.com','123',1,3);
INSERT INTO usuario VALUES(0,'Juan Carlos', 'Ruiz Nativi','juan@gmail.com','123',1,3);


INSERT INTO usuario VALUES(0,'Jocelyn Stefany', 'de Paz Castellanos','joss@gmail.com','123',2,4); -- Encargado ID 2
INSERT INTO usuario VALUES(0,'Azucena del Carmen', 'Colocho','azu@gmail.com','123',null,4); -- Para Asignar a ID 2
INSERT INTO usuario VALUES(0,'Christian Denilson', 'Ramirez Sarmiento','chris@gmail.com','123',null,4); -- Para Asignar a ID 2

INSERT INTO usuario VALUES(0,'Silvia Andrea', 'Estupinian Ungo','silvia@gmail.com','123',3,4); -- Encargado ID 3
INSERT INTO usuario VALUES(0,'Erick Alexis', 'Garcia Peña','erick@gmail.com','123',null,4); -- Para Asignar a ID 3
INSERT INTO usuario VALUES(0,'Josue Alexander', 'Escobar','josue@gmail.com','123',null,4); -- Para Asignar a ID 3

INSERT INTO usuario VALUES(0,'Rosmary', 'Garcia', 'ros@gmail.com', '123', null,2); -- Encargada de RRHH


SELECT * FROM usuario WHERE subornidado IS NULL AND id_tipousuario > 2; -- Empleados que no tiene subornidado y no muestra usuario tipo RH;
SELECT * FROM usuario WHERE id_tipousuario <> 2 AND id_tipousuario < 4; -- Empleados tipo Jefe;

SELECT * FROM usuario WHERE id_tipousuario <> 2;

UPDATE usuario SET subornidado = 2 , id_tipousuario = 2 WHERE id_tipousuario = 2;


SELECT * FROM usuario WHERE subornidado = 1;

SELECT * FROM asignaciones WHERE id_usuario = 2;
SELECT * FROM evento WHERE id_usuario = 2;



SELECT a.id_usuario, e.titulo, a.estado FROM asignaciones a
INNER JOIN evento e ON e.id_evento = a.id_evento 
WHERE e.id_usuario = 2;



-- Cantidad de Tareas Asinadas a Mis empleados
SELECT COUNT(a.estado) AS 'Tareas Realizadas' FROM asignaciones a
INNER JOIN evento e ON e.id_evento = a.id_evento 
WHERE e.id_usuario = 1;

-- Cantidad de Tareas Asinadas a Mis empleados
SELECT COUNT(a.estado) AS 'Tareas Realizadas' FROM asignaciones a
INNER JOIN evento e ON e.id_evento = a.id_evento 
WHERE e.id_usuario = 2 AND a.estado = 'Realizado';

-- Cantidad de Tareas Asinadas a Mis empleados
SELECT COUNT(a.estado) AS 'Tareas Realizadas' FROM asignaciones a
INNER JOIN evento e ON e.id_evento = a.id_evento 
WHERE e.id_usuario = 2 AND a.estado = 'Pendiente';

-- Cantidad de Tareas Asinadas a mis emeplados(empleado seleccionado)
SELECT COUNT(a.estado), e.id_evento, a.estado, u.nombre, e.titulo FROM asignaciones a
INNER JOIN evento e ON e.id_evento = a.id_evento 
INNER JOIN usuario u ON u.id_usuario = a.id_usuario
WHERE e.id_usuario = 1 AND a.estado = 'Pendiente'
GROUP BY a.id_usuario;


SELECT e.id_evento, a.estado, u.nombre, e.titulo FROM asignaciones a
INNER JOIN evento e ON e.id_evento = a.id_evento 
INNER JOIN usuario u ON u.id_usuario = a.id_usuario
WHERE e.id_usuario = 1 AND a.estado = 'Pendiente'
GROUP BY a.id_usuario;


SELECT * FROM evento e
INNER JOIN usuario u ON u.id_usuario = e.id_usuario
WHERE e.id_usuario = 2;

SELECT * FROM evento e
INNER JOIN usuario u ON u.id_usuario = e.id_usuario
WHERE e.id_usuario = 2 AND e.estado = 'Realizado';

SELECT * FROM evento e
INNER JOIN usuario u ON u.id_usuario = e.id_usuario
WHERE e.id_usuario = 2 AND e.estado = 'Pendiente';

SELECT * FROM asignaciones WHERE id_usuario = 2 OR id_usuario = 3;
SELECT * FROM nota;

