package com.cs.models;

import com.cs.models.Joven;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2020-02-20T09:37:06")
@StaticMetamodel(Hobby.class)
public class Hobby_ { 

    public static volatile SingularAttribute<Hobby, Joven> joven;
    public static volatile SingularAttribute<Hobby, Integer> idHobby;
    public static volatile SingularAttribute<Hobby, String> nombre;

}