/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.utils;

/**
 *
 * @author christian.ramirezusa
 */

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public abstract class AbstractFacade<T> {
    private Class<T> entityClass;
    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
    protected abstract EntityManager getEntityManager();
    
    public void create(T entity) {
        EntityTransaction t = getEntityManager().getTransaction();
        try {
            t.begin();
            getEntityManager().persist(entity);
            t.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            getEntityManager().clear();
        }
    }
    public void edit(T entity) {
        EntityTransaction t = getEntityManager().getTransaction();
        try {
            t.begin();
            getEntityManager().merge(entity);
            t.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            getEntityManager().clear();
        }
    }
    public void remove(T entity) {
        EntityTransaction t = getEntityManager().getTransaction();
        try {
            t.begin();
            getEntityManager().remove(getEntityManager().merge(entity));
            t.commit();
        } catch (Exception e) {
            throw e;
        } finally {
            getEntityManager().clear();
        }
    }
    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }
}