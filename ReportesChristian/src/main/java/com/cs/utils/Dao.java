/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.utils;

import java.util.List;

/**
 *
 * @author christian.ramirezusa
 */
public interface Dao<T> {
    public void create(T c);
    public void remove(T c);
    public void edit(T c);
    public List<T> findAll();
}
