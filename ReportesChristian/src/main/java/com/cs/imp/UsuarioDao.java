/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.imp;

import com.cs.models.Joven;
import com.cs.models.Usuario;
import com.cs.utils.AbstractFacade;
import com.cs.utils.Dao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author christian.ramirezusa
 */
public class UsuarioDao extends AbstractFacade<Usuario> implements Dao<Usuario>{
    
    EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public UsuarioDao() {
        super(Usuario.class);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("reportPU");
        em = emf.createEntityManager();
    }
    
    public List<Usuario> login(String usuario, String pass){
        Query query = em.createNamedQuery("Login");
        try {            
            query.setParameter(1, usuario);
            query.setParameter(2, pass);            
            List<Usuario> list = query.getResultList();
            return list;
        } catch (Exception e) {
            System.out.println("Error Dao: " +e.getMessage());
            return null;
        }
    }
}
