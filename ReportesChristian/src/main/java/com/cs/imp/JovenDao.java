/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.imp;

import com.cs.models.Joven;
import com.cs.utils.AbstractFacade;
import com.cs.utils.Dao;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author christian.ramirezusa
 */
public class JovenDao extends AbstractFacade<Joven> implements Dao<Joven>{

    EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public JovenDao() {
        super(Joven.class);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("reportPU");
        em = emf.createEntityManager();
    }

    
    
    
}
