/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.control;

import com.cs.imp.HobbyDao;
import com.cs.imp.JovenDao;
import com.cs.imp.UsuarioDao;
import com.cs.models.Hobby;
import com.cs.models.Joven;
import com.cs.models.Usuario;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author christian.ramirezusa
 */
@ManagedBean
public class JovenesAfi implements Serializable {

    private UsuarioDao usuarioDao = new UsuarioDao();
    private JovenDao jovenDao = new JovenDao();
    private HobbyDao hobbyDao = new HobbyDao();
    private Usuario usuario;
    private Joven joven;
    private Hobby hobby;
    private List<Joven> listaJovenes;

    public List<Joven> getListaJovenes() {
        return listaJovenes = jovenDao.findAll();
    }

    public void setListaJovenes(List<Joven> listaJovenes) {
        this.listaJovenes = listaJovenes;
    }
    
    

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Joven getJoven() {
        return joven;
    }

    public void setJoven(Joven joven) {
        this.joven = joven;
    }

    public Hobby getHobby() {
        return hobby;
    }

    public void setHobby(Hobby hobby) {
        this.hobby = hobby;
    }

    @PostConstruct
    public void init() {
        usuarioDao = new UsuarioDao();
        usuario = new Usuario();
        joven = new Joven();
        hobby = new Hobby();
    }

    public void login() throws IOException {
        String ruta = null;

        List<Usuario> lista = usuarioDao.login(usuario.getUser(), usuario.getPass());
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

        try {
            if (lista.isEmpty()) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/ReportesChristian/faces/index.xhtml");
            } else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/ReportesChristian/faces/menu.xhtml");
            }
        } catch (Exception e) {
            System.out.println("Error: " +e.getMessage() );
        }

    }
    
    public void agregarHobby(){
    
        try {            
            hobby.setJoven(joven);
            hobbyDao.create(hobby);
            FacesMessage msg = new FacesMessage("Agregado");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
        }
    }
}
