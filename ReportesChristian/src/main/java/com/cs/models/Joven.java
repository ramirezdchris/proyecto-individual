/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cs.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author christian.ramirezusa
 */
@Entity
@Table(name = "joven")
public class Joven implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "joven_id")
    private int idJoven;
    
    @Column(name = "nombre")
    private String nombre;

    public int getJovenId() {
        return idJoven;
    }

    public void setJovenId(int idJoven) {
        this.idJoven = idJoven;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.idJoven;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Joven other = (Joven) obj;
        if (this.idJoven != other.idJoven) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Joven{" + "idJoven=" + idJoven + '}';
    }
    
}
