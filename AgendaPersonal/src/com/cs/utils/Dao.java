package com.cs.utils;

import java.util.List;

public interface Dao<T> {

	public void create(T e);
	
	public List<T> read();
	
	public T readById(Object id);
	
	public void update(T e);
	
	public void delete(T e);
}
