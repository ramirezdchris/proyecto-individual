package com.cs.utils;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class Inicializacion extends AbstractAnnotationConfigDispatcherServletInitializer{

	
	protected Class<?>[] getRootConfigClasses(){
		return new Class[] {Configuracion.class};
	}
	
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}
	
	protected String[] getServletMappings() {
		return new String[] {"/"};
	}

}
