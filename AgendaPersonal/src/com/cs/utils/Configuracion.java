package com.cs.utils;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.cs.imp.AsignacionesDao;
import com.cs.imp.EventoDao;
import com.cs.imp.NotaDao;
import com.cs.imp.PrioridadDao;
import com.cs.imp.RendimientoDao;
import com.cs.imp.UsuarioDao;

@Configuration
@EnableWebMvc
@EnableTransactionManagement(proxyTargetClass = true)
@ComponentScan({"com.cs"})
public class Configuracion {
	
	@Bean
	public InternalResourceViewResolver vistas(){
		InternalResourceViewResolver resultado = new InternalResourceViewResolver();
		resultado.setPrefix("/");
		resultado.setSuffix(".jsp");
		return resultado;
	}
	
	@Bean(name = "dataSource")
	public DataSource dataSource(){
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/agendapersonal?useSSL=false");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}
	
	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessiocFactory(DataSource dataSource){
		LocalSessionFactoryBuilder fabrica = new LocalSessionFactoryBuilder(dataSource);
		fabrica.scanPackages("com.cs.models");
		fabrica.addProperties(getHibernateProperties());
		return fabrica.buildSessionFactory();
	}
	
	@Autowired
	@Bean(name = "trasactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
			HibernateTransactionManager tx = new HibernateTransactionManager(sessionFactory);
			return tx;
	}
	
	
	// Primero hacer metodo getHibernateProperties para hacer el getHibernateProperties
	private Properties getHibernateProperties() {
		Properties hibernateProperties = new Properties();
		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return hibernateProperties;
	}
	
	
	@Autowired
	@Bean(name = "UsuariosDao")
	public UsuarioDao usuarioRep(SessionFactory sessionFactory) {
		return new UsuarioDao(sessionFactory);
	}

	
	@Autowired
	@Bean(name = "PrioridadDao")
	public PrioridadDao prioridadRep(SessionFactory sessionFactory) {
		return new PrioridadDao(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "EventoDao")
	public EventoDao eventoRep(SessionFactory sessionFactory) {
		return new EventoDao(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "NotaDao")
	public NotaDao notaRep(SessionFactory sessionFactory) {
		return new NotaDao(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "RendimientoDao")
	public RendimientoDao rendimientoReq(SessionFactory sessionFactory) {
		return new RendimientoDao(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "AsignacionesDao")
	public AsignacionesDao asignacionesReq(SessionFactory sessionFactory) {
		return new AsignacionesDao(sessionFactory);
	}
	
	

}
