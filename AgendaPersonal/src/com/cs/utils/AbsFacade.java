package com.cs.utils;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbsFacade<T> {
	
	private Class<T> entityClass;
	
	public AbsFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	@Autowired
	public abstract SessionFactory sessionFactory();
	
	protected Session session;
	
	@Transactional
	public void create(T entity) {
		session = sessionFactory().getCurrentSession();
		session.save(entity);
	}
	
	@Transactional
	public void update(T entity) {
		session = sessionFactory().getCurrentSession();
		session.update(entity);
	}
	
	@Transactional
	public void delete(T entity) {
		session = sessionFactory().getCurrentSession();
		session.delete(entity);
	}
	
	@Transactional
	public T readById(Object id) {
		session = sessionFactory().getCurrentSession();
		return session.find(entityClass, id);
	}
	
	@Transactional
	public List<T> read(){
		session = sessionFactory().getCurrentSession();
		CriteriaQuery cq = session.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return session.createQuery(cq).getResultList();
	}
}
