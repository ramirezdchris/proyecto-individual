package com.cs.models;
// Generated 03-03-2020 08:36:17 AM by Hibernate Tools 5.2.12.Final

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Usuario generated by hbm2java
 */
@Entity
@Table(name = "usuario", catalog = "agendapersonal")
public class Usuario implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idUsuario;
	private Tipousuario tipousuario;
	private Usuario usuario;
	private String nombre;
	private String apellido;
	private String email;
	private String pass;
	private List<Asignaciones> asignacioneses = new ArrayList<>();
	private List<Evento>eventos = new ArrayList<>();
	private List<Usuario> usuarios = new ArrayList<>();
	private List<Rendimiento>rendimientos = new ArrayList<>();

	public Usuario() {
	}

	public Usuario(Tipousuario tipousuario, String nombre, String apellido, String email, String pass) {
		this.tipousuario = tipousuario;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.pass = pass;
	}

	public Usuario(Tipousuario tipousuario, Usuario usuario, String nombre, String apellido, String email, String pass,
			List<Asignaciones> asignacioneses, List<Evento> eventos, List<Usuario> usuarios, List<Rendimiento> rendimientos) {
		this.tipousuario = tipousuario;
		this.usuario = usuario;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.pass = pass;
		this.asignacioneses = asignacioneses;
		this.eventos = eventos;
		this.usuarios = usuarios;
		this.rendimientos = rendimientos;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id_usuario", unique = true, nullable = false)
	public Integer getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tipousuario", nullable = false)
	public Tipousuario getTipousuario() {
		return this.tipousuario;
	}

	public void setTipousuario(Tipousuario tipousuario) {
		this.tipousuario = tipousuario;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "subornidado")
	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Column(name = "nombre", nullable = false, length = 30)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "apellido", nullable = false, length = 30)
	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Column(name = "email", nullable = false, length = 50)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "pass", nullable = false, length = 50)
	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
	public List<Asignaciones> getAsignacioneses() {
		return this.asignacioneses;
	}

	public void setAsignacioneses(List<Asignaciones> asignacioneses) {
		this.asignacioneses = asignacioneses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
	public List<Evento> getEventos() {
		return this.eventos;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
	public List<Usuario> getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
	public List<Rendimiento> getRendimientos() {
		return this.rendimientos;
	}

	public void setRendimientos(List<Rendimiento> rendimientos) {
		this.rendimientos = rendimientos;
	}

}
