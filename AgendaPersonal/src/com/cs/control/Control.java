package com.cs.control;

import java.awt.PageAttributes.MediaType;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.JsonbHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.cs.imp.AsignacionesDao;
import com.cs.imp.EventoDao;
import com.cs.imp.NotaDao;
import com.cs.imp.PrioridadDao;
import com.cs.imp.UsuarioDao;
import com.cs.models.Asignaciones;
import com.cs.models.AsignacionesId;
import com.cs.models.Evento;
import com.cs.models.Nota;
import com.cs.models.Prioridad;
import com.cs.models.Tipousuario;
import com.cs.models.Usuario;

@Controller
public class Control {

	@Autowired
	@Qualifier("usuarioDao")
	private UsuarioDao usuarioDao;

	@Autowired
	@Qualifier("prioridadDao")
	private PrioridadDao prioridadDao;

	@Autowired
	@Qualifier("eventoDao")
	private EventoDao eventoDao;

	@Autowired
	@Qualifier("asignacionesDao")
	private AsignacionesDao asignacionesDao;

	@Autowired
	@Qualifier("notaDao")
	private NotaDao notaDao;

	// private Usuario user;

	private String error;
	private String msg;
	private String vista;

	/*
	 * @RequestMapping(value = "/registroUsuario" , method = RequestMethod.GET)
	 * public ModelAndView registroUsuario() { String msj = "Registro insertado";
	 * ModelAndView mv = new ModelAndView(); mv.addObject(msj);
	 * mv.setViewName("index"); return mv; }
	 */

	@RequestMapping(value = "/registroUsuario", method = RequestMethod.GET)
	public ModelAndView registroUsuario(@RequestParam("nombre") String nombre,
			@RequestParam("apellido") String apellido, @RequestParam("email") String email,
			@RequestParam("pass") String pass) {

		ModelAndView mv = new ModelAndView();

		try {
			Usuario usu = new Usuario();
			Tipousuario tiusu = new Tipousuario();
			usu.setIdUsuario(0);
			usu.setNombre(nombre);
			usu.setApellido(apellido);
			usu.setEmail(email);
			usu.setPass(pass);
			tiusu.setIdTipousuario(4);
			// usu.setUsuario(usu);
			usu.setTipousuario(tiusu);
			usuarioDao.create(usu);

			msg = "Bienvenido " + usu.getNombre() + " " + usu.getApellido() + "<br>" + "a Tu Agenda Personal";
			mv.addObject("msg", msg);

			// String box = "<div class='gradient-border' id='box' <br>" +msg +"<br></div>";
			// mv.addObject("box", box);

			mv.addObject("msg", "Bienevenido");
			mv.setViewName("index");
			usu = new Usuario();
			return mv;
		} catch (Exception e) {
			System.out.println("Error al registrarse: " + e.getMessage());
			error = "Error al registrarse";
			this.vista = "index";
			return mv;
		}

	}

	@RequestMapping(value = "/verUsuarios", method = RequestMethod.GET)
	public ModelAndView consultaUsuarios() {
		ModelAndView mv = new ModelAndView();
		try {
			List<Usuario> list = new ArrayList<Usuario>();
			list = usuarioDao.read();
			mv.addObject("lista", list);
			mv.setViewName("index");
			return mv;
		} catch (Exception e) {
			error = "Error al registrarse";
			this.vista = "index";
			return mv;
		}
	}

	@RequestMapping(value = "/eventos", method = RequestMethod.GET)
	public ModelAndView registroLista(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		HttpSession session = request.getSession();
		try {
			int idUsuario = (int) session.getAttribute("idUsuario");
			session.getAttribute("idUsuario");
			List<Prioridad> listaPrio = new ArrayList<>();
			List<Evento> listaEventos = eventoDao.listaEventosPendientes(idUsuario);
			List<Asignaciones> listaEventosAsignados = asignacionesDao.listaAsignacionesPendientes(idUsuario);
			List<Usuario> listaSubor = usuarioDao.empCargo(idUsuario);

			// Todas las Tareas
			List<Evento> listaEventosTodos = eventoDao.listaEventos(idUsuario);
			List<Asignaciones> listaEventosAsignadosTodos = asignacionesDao.listaAsignaciones(idUsuario);
			mv.addObject("listaEventosTodos", listaEventosTodos);
			mv.addObject("listaEventosAsignadosTodos", listaEventosAsignadosTodos);

			listaPrio = prioridadDao.read();
			mv.addObject("listaPrio", listaPrio);
			mv.addObject("listaEventos", listaEventos);
			mv.addObject("listaEventosAsignados", listaEventosAsignados);
			mv.addObject("listaSubor", listaSubor);
			mv.setViewName("registareas");
			return mv;
		} catch (Exception e) {
			error = "Error al registrarse";
			this.vista = "index";
			System.out.println("Error Control: " + e.getMessage());
			return mv;
		}
	}

	@RequestMapping(value = "/notas", method = RequestMethod.GET)
	public ModelAndView notas(HttpServletRequest request, @RequestParam("evento") int idEvento) {
		ModelAndView mv = new ModelAndView();
		HttpSession session = request.getSession();
		try {
			int idUsuario = (int) session.getAttribute("idUsuario");
			session.getAttribute("idUsuario");
			List<Nota> listaNotaEvento = notaDao.notasEvento(idEvento);

			if (listaNotaEvento == null) {
				mv.addObject("idEvento", idEvento);
				mv.addObject("nombreEvento", "No Hay Eventos");
				mv.addObject("listaNotas", listaNotaEvento);
			} else {
				mv.addObject("idEvento", idEvento);
				mv.addObject("nombreEvento", listaNotaEvento.get(0).getEvento().getTitulo());
				System.out.println(listaNotaEvento.get(0).getEvento().getTitulo());
				mv.addObject("listaNotas", listaNotaEvento);
			}

			mv.setViewName("notas");
			return mv;
		} catch (Exception e) {
			error = "Error al registrarse";
			this.vista = "index";
			System.out.println("Error Control Metodo NOTAS: " + e.getMessage());
			return mv;
		}
	}

	@RequestMapping(value = "/eventosUsuario", method = RequestMethod.GET)
	public ModelAndView registroListaAsisgnadas(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		HttpSession session = request.getSession();
		try {
			int idUsuario = (int) session.getAttribute("idUsuario");
			session.getAttribute("idUsuario");
			List<Prioridad> listaPrio = prioridadDao.read();
			List<Asignaciones> listaEventosAsignados = asignacionesDao.listaAsignaciones(idUsuario);
			List<Usuario> listaSubor = usuarioDao.empCargo(idUsuario);

			List<Evento> listaEventos = eventoDao.listaEventosPendientes(idUsuario);
			mv.addObject("listaEventos", listaEventos);

			mv.addObject("listaPrio", listaPrio);
			mv.addObject("listaEventosAsignados", listaEventosAsignados);
			mv.addObject("listaSubor", listaSubor);
			mv.setViewName("registareasUsuarios");
			return mv;
		} catch (Exception e) {
			error = "Error al registrarse";
			this.vista = "index";
			System.out.println("Error Control: " + e.getMessage());
			return mv;
		}
	}

	@RequestMapping(value = "/registroEvento", method = RequestMethod.GET)
	public String registroTarea(@RequestParam("titulo") String titulo, @RequestParam("descripcion") String descripcion,
			@RequestParam("fechaInicio") String fechaInicio, @RequestParam("fechaFin") String fechaFin,
			@RequestParam("horaInicio") String horaInicio, @RequestParam("horaFin") String horaFin,
			@RequestParam("prioridad") int prioridad, @RequestParam("usuario") int usuario) {
		ModelAndView mv = new ModelAndView();

		try {
			Evento evento = new Evento();
			Prioridad prio = new Prioridad();
			Usuario usu = new Usuario();

			SimpleDateFormat ffecha = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat fhora = new SimpleDateFormat("HH:mm");

			usu.setIdUsuario(usuario);
			prio.setIdPrioridad(prioridad);

			evento.setIdEvento(0);
			evento.setTitulo(titulo);
			evento.setDescripcion(descripcion);
			evento.setFechaInicio(ffecha.parse(fechaInicio));
			evento.setFechaFin(ffecha.parse(fechaFin));
			evento.setHoraInicio(fhora.parse(horaInicio));
			evento.setHoraFin(fhora.parse(horaFin));
			evento.setEstado("Pendiente");
			evento.setUsuario(usu);
			evento.setPrioridad(prio);

			eventoDao.create(evento);
			System.out.println("Agregada");
//			mv.addObject("msg" , "Tarea Agregada");
//			registroLista();
			// String precio;
			// Double.parseDouble(precio);

			return "redirect:/eventos";
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			return "redirect:/index";
		}

	}

	@RequestMapping(value = "/agregarNota", method = RequestMethod.GET)
	public String agregarNota(@RequestParam("nota") String nota, @RequestParam("evento") int idEvento) {
		ModelAndView mv = new ModelAndView();

		try {
			Evento evento = new Evento();
			Prioridad prio = new Prioridad();
			Usuario usu = new Usuario();
			Nota n = new Nota();

			n.setIdNota(0);
			n.setNota(nota);
			evento.setIdEvento(idEvento);
			n.setEvento(evento);

			notaDao.create(n);
			System.out.println("Agregada");
//			mv.addObject("msg" , "Tarea Agregada");
//			registroLista();
			// String precio;
			// Double.parseDouble(precio);

			return "redirect:/notas?evento=" + idEvento;
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			return "redirect:/index";
		}

	}

	@RequestMapping(value = "/asignarTareas", method = RequestMethod.GET)
	public String asignarTareas(@RequestParam("empleados") String[] emple, @RequestParam("evento") int idEvento) {
		ModelAndView mv = new ModelAndView();

		try {
			Evento evento = new Evento();
			Prioridad prio = new Prioridad();
			Usuario usu = new Usuario();

			AsignacionesId asigId = new AsignacionesId();
			Asignaciones asig = new Asignaciones();

			for (int i = 0; i < emple.length; i++) {
				System.out.println("IDD: " + emple[i]);

				int idEmpleado = Integer.parseInt(emple[i]);

				asigId.setIdEvento(idEvento);
				asigId.setIdUsuario(idEmpleado);
				asigId.setEstado("Pendiente");

				evento.setIdEvento(idEvento);
				usu.setIdUsuario(idEmpleado);

				asig.setEvento(evento);
				asig.setUsuario(usu);

				asig.setId(asigId);

				asignacionesDao.create(asig);

			}
			// System.out.println("ID: " +idEmpleado);
			/*
			 * SimpleDateFormat ffecha = new SimpleDateFormat("yyyy-MM-dd");
			 * SimpleDateFormat fhora = new SimpleDateFormat("HH:mm");
			 * 
			 * usu.setIdUsuario(usuario); prio.setIdPrioridad(prioridad);
			 * 
			 * evento.setIdEvento(0); evento.setTitulo(titulo);
			 * evento.setDescripcion(descripcion);
			 * evento.setFechaInicio(ffecha.parse(fechaInicio));
			 * evento.setFechaFin(ffecha.parse(fechaFin));
			 * evento.setHoraInicio(fhora.parse(horaInicio));
			 * evento.setHoraFin(fhora.parse(horaFin)); evento.setEstado("Pendiente");
			 * evento.setUsuario(usu); evento.setPrioridad(prio);
			 * 
			 * eventoDao.create(evento); System.out.println("Agregada"); //
			 * mv.addObject("msg" , "Tarea Agregada"); // registroLista(); //String precio;
			 * //Double.parseDouble(precio);
			 */
			return "redirect:/eventos";
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			return "redirect:/index";
		}

	}

	@RequestMapping(value = "/rendimiento", method = RequestMethod.GET)
	public ModelAndView rendimiento(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		HttpSession session = request.getSession();
		List<Evento> ePP = eventoDao.totalEventosPendientes(2);
		
		try {
			int idUsuario = (int) session.getAttribute("idUsuario");
			
			List<Evento> eT = eventoDao.totalEventos(idUsuario);
			List<Evento> eR = eventoDao.totalEventosRealizados(idUsuario);
			List<Evento> eP = eventoDao.totalEventosPendientes(idUsuario);
			int tareasRealizadas = eR.size();
			int tareasPendientes = eP.size();
			int totalTareas = eT.size();
			
			mv.addObject("totalEvento", totalTareas);
			mv.addObject("tareaRealizada", tareasRealizadas);
			mv.addObject("tareaPendiente", tareasPendientes);
			mv.setViewName("rendimiento");
			return mv;

		} catch (Exception e) {

			System.out.println("Error Metodo Rendimiento" + e.getMessage() +ePP.size());
			return new ModelAndView("redirect:/index.jsp");
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam("email") String email, @RequestParam("pass") String pass,
			HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		HttpSession session = request.getSession();
		try {

			// Usuario usu = new Usuario();
			// usu.setEmail(email);
			// usu.setPass(pass);
			List<Usuario> lista = usuarioDao.login(email, pass);

			if (lista.isEmpty()) {
				mv.addObject("msg", "Credenciales invalidas");
				mv.setViewName("index");
				return mv;
			} else {
				session.setAttribute("usuario", lista.get(0).getIdUsuario());
				session.setAttribute("user", lista.get(0).getNombre() + " " + lista.get(0).getApellido());
				session.setAttribute("idUsuario", lista.get(0).getIdUsuario());
				session.setAttribute("rol", lista.get(0).getTipousuario().getIdTipousuario());
				return new ModelAndView("redirect:/eventos");
			}
//			

		} catch (Exception e) {
			System.out.println("Error" + email);
			System.out.println("Error" + pass);
			System.out.println("Error" + e.getMessage());
			return new ModelAndView("redirect:/index.jsp");
		}
	}

	@RequestMapping(value = "/eventoEstado", method = RequestMethod.GET)
	public ModelAndView eventoEstado(@RequestParam("idEvento") int idEvento) {
		try {
			eventoDao.eventoRealizado(idEvento);
			return new ModelAndView("redirect:/eventos");
		} catch (Exception e) {
			// TODO: handle exception
			return new ModelAndView("redirect:/eventos");
		}
	}

	@RequestMapping(value = "/asignacionesEstado", method = RequestMethod.GET)
	public ModelAndView asignacionesEstado(@RequestParam("idEvento") int idEvento, HttpServletRequest request) {
		HttpSession session = request.getSession();
		try {
			int idUsuario = (int) session.getAttribute("idUsuario");
			asignacionesDao.asignacionRealizada(idEvento, idUsuario);
			return new ModelAndView("redirect:/eventos");
		} catch (Exception e) {
			// TODO: handle exception
			return new ModelAndView("redirect:/eventos");
		}
	}

	@RequestMapping(value = "/salir", method = RequestMethod.GET)
	public ModelAndView salir(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView();
		HttpSession session = request.getSession(false);

		if (session != null) {
			session.invalidate();
			mv.setViewName("index");
			return mv;
		}
		return null;
	}

	@RequestMapping(value = "findall")
	public String JSON() {
		return JSONObject.quote("Hola");
	}

	@RequestMapping(value = "/getString", produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	public Map getString() {
		return Collections.singletonMap("response", "Hello World");
	}

}
