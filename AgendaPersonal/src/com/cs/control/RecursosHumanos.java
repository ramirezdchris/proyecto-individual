package com.cs.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cs.imp.AsignacionesDao;
import com.cs.imp.EventoDao;
import com.cs.imp.PrioridadDao;
import com.cs.imp.UsuarioDao;
import com.cs.models.Usuario;

@Controller
public class RecursosHumanos {

	
	@Autowired
	@Qualifier("usuarioDao")
	private UsuarioDao usuarioDao;
	
	@Autowired
	@Qualifier("prioridadDao")
	private PrioridadDao prioridadDao;
	
	@Autowired
	@Qualifier("eventoDao")
	private EventoDao eventoDao;
	
	@Autowired
	@Qualifier("asignacionesDao")
	private AsignacionesDao asignacionesDao;
	
	/*@RequestMapping(value = "/listaEmple" , method = RequestMethod.GET)
	public ModelAndView listaEmpleados(){
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}*/
	
	@RequestMapping(value = "/listasEmpleados" , method = RequestMethod.GET)
	public ModelAndView listas() {
		ModelAndView mv = new ModelAndView();
		List<Usuario> list;
		List<Usuario> listJefes;
		try {
			list = usuarioDao.empSinSub();
			listJefes = usuarioDao.empJefe();
			mv.addObject("listEmpSin" , list);
			mv.addObject("listEmpJefe" , listJefes);
			mv.setViewName("rh");
			return mv;
		} catch (Exception e) {
			return mv;
		}
	}
	
	@RequestMapping(value = "/actualizar" , method = RequestMethod.GET)
	public ModelAndView actualizar(@RequestParam("idSubor") int idSubor, @RequestParam("idJefe") int idJefe, @RequestParam("cargoSubor") int cargoSubor) {
		ModelAndView mv;
		try {
			usuarioDao.actualizar(idSubor, idJefe, cargoSubor);
			return mv = new ModelAndView("redirect:/listasEmpleados");
		} catch (Exception e) {
			// TODO: handle exception
			return mv = new ModelAndView("redirect:/listasEmpleados");
		}
	}
	
	
}
