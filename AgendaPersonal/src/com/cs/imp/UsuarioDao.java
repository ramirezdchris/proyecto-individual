package com.cs.imp;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cs.models.Usuario;
import com.cs.utils.AbsFacade;
import com.cs.utils.Dao;

@Repository
@Transactional
public class UsuarioDao extends AbsFacade<Usuario> implements Dao<Usuario>{

	@Autowired
	private SessionFactory startSession;
	
	public UsuarioDao(SessionFactory sessionFactory) {
		super(Usuario.class);
		this.startSession = sessionFactory;
	}

	@Override
	public SessionFactory sessionFactory() {
		return startSession;		
	}

	
	public List<Usuario> login(String email, String pass){
		Query q;
		try {
			q = sessionFactory().getCurrentSession().createQuery("SELECT U FROM Usuario U WHERE U.email=?1 AND U.pass=?2");
			q.setParameter(1, email);
			q.setParameter(2, pass);
			List<Usuario> lista = q.getResultList();
			return lista;
		} catch (Exception e) {
			System.out.println("Error DAO Usuario: " +e.getMessage());
			return null;
		}
	}
	
	public List<Usuario> empSinSub(){
		Query q;
		
		try {
			
			// Empleados que no tiene subornidado y no muestra usuario tipo RH
			
			q = sessionFactory().getCurrentSession().createNativeQuery("SELECT * FROM usuario WHERE subornidado IS NULL AND id_tipousuario > 2;" , Usuario.class);
			List<Usuario> lista = q.getResultList();
			return lista;
		} catch (Exception e) {
			System.out.println("Error DAO Usuario Metodo empSinSub: " +e.getMessage());
			return null;
		}
	}
	
	public List<Usuario> empJefe(){
		Query q;
		
		try {
			
			// Empleados tipo Jefe;
			
			q = sessionFactory().getCurrentSession().createNativeQuery("SELECT * FROM usuario WHERE id_tipousuario <> 2 AND id_tipousuario < 4;" , Usuario.class);
			List<Usuario> lista = q.getResultList();
			return lista;
		} catch (Exception e) {
			System.out.println("Error DAO Usuario Metodo empSinSub: " +e.getMessage());
			return null;
		}
	}
	
	public boolean actualizar(int idUsuario, int idJefe, int idTipo) {
		Query q;
		try {
			q = sessionFactory().getCurrentSession().createNativeQuery("UPDATE usuario SET subornidado = "+ idJefe +"," +"id_tipousuario = " +idTipo +" WHERE id_usuario ="+idUsuario +";");
			q.executeUpdate();
			System.out.println("Dentro");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error DAO Usuario Metodo actualizar: " +e.getMessage());
			return false;
		}
	}
	
	public List<Usuario> empCargo(int idJefe){
		Query q;
		
		try {
			
			// Empleados tipo Jefe;
			
			q = sessionFactory().getCurrentSession().createNativeQuery("SELECT * FROM usuario WHERE subornidado = "+idJefe +";" , Usuario.class);
			List<Usuario> lista = q.getResultList();
			return lista;
		} catch (Exception e) {
			System.out.println("Error DAO Usuario Metodo empCargo: " +e.getMessage());
			return null;
		}
	}
}
