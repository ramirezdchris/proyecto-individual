package com.cs.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cs.models.Prioridad;
import com.cs.models.Usuario;
import com.cs.utils.AbsFacade;
import com.cs.utils.Dao;

@Repository
@Transactional
public class PrioridadDao extends AbsFacade<Prioridad> implements Dao<Prioridad> {

	@Autowired
	private SessionFactory startSession;

	public PrioridadDao(SessionFactory sessionFactory) {
		super(Prioridad.class);
		this.startSession = sessionFactory;
	}

	@Override
	public SessionFactory sessionFactory() {
		return startSession;
	}

}
