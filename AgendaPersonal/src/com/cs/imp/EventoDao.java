package com.cs.imp;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cs.models.Asignaciones;
import com.cs.models.Evento;
import com.cs.utils.AbsFacade;
import com.cs.utils.Dao;

@Repository
@Transactional
public class EventoDao extends AbsFacade<Evento> implements Dao<Evento>{

	@Autowired
	private SessionFactory startSession;
	
	public EventoDao(SessionFactory sessionFactory) {
		super(Evento.class);
		this.startSession = sessionFactory;
	}

	@Override
	public SessionFactory sessionFactory() {
		return startSession;		
	}
	
	public List<Evento> listaEventosPendientes(int idUsuario){
		Query q;
		try {
			q = sessionFactory().getCurrentSession().createQuery("SELECT E FROM Evento E JOIN E.usuario USER WHERE USER.idUsuario=?1 AND E.estado = 'Pendiente'");
			q.setParameter(1, idUsuario);
			List<Evento> list = q.getResultList();
			
			return list;
		} catch (Exception e) {
			System.out.println("Error DAO Evento: " +e.getMessage());
			return null;
		}
	}

	public void eventoRealizado(int idEvento) {
		Query q;
		try {
			q = sessionFactory().getCurrentSession().createNativeQuery("UPDATE evento SET estado = 'Realizado' WHERE id_evento = " +idEvento +";"); 
			q.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error en DAO Evento Metodo eventoRealizada" +e.getMessage());
		}
		
	}
	
	public List<Evento> listaEventos(int idUsuario){
		Query q;
		try {
			q = sessionFactory().getCurrentSession().createQuery("SELECT E FROM Evento E JOIN E.usuario USER WHERE USER.idUsuario=?1");
			q.setParameter(1, idUsuario);
			List<Evento> list = q.getResultList();
			
			return list;
		} catch (Exception e) {
			System.out.println("Error DAO Evento: " +e.getMessage());
			return null;
		}
	}
	
	public List<Evento> totalEventos(int idUsuario) {
		Query q;
		List<Evento> lista = new ArrayList<Evento>();
		try {
			q = sessionFactory().getCurrentSession().createNativeQuery("SELECT * FROM evento e " + "INNER JOIN usuario u ON u.id_usuario = e.id_usuario" + " WHERE e.id_usuario = " +idUsuario+";" , Evento.class);			
			return lista = q.getResultList();
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error en DAO Evento Metodo totalEventos" +e.getMessage());
			return null;
		}
	}
	
	public List<Evento> totalEventosRealizados(int idUsuario) {
		Query q;
		List<Evento> lista = new ArrayList<Evento>();
		try {
			q = sessionFactory().getCurrentSession().createNativeQuery("SELECT * FROM evento e INNER JOIN usuario u ON u.id_usuario = e.id_usuario WHERE e.id_usuario = " +idUsuario+" AND e.estado = 'Realizado';" , Evento.class);
			return lista = q.getResultList();
			//int var = lista.get(0).getIdEvento(); 
			//return var;
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error en DAO Evento Metodo totalEventosRealizados " +e.getMessage());
			return null;
		}
	}
	
	public List<Evento> totalEventosPendientes(int idUsuario) {
		Query q;
		List<Evento> lista = new ArrayList<Evento>();
		try {
			q = sessionFactory().getCurrentSession().createNativeQuery("SELECT * FROM evento e INNER JOIN usuario u ON u.id_usuario = e.id_usuario WHERE e.id_usuario = " +idUsuario+" AND e.estado = 'Pendiente';" , Evento.class);
			return lista = q.getResultList();
			//int var = lista.get(0).getIdEvento(); 
			//return var;
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error en DAO Evento Metodo totalEventosPendientes " +e.getMessage());
			return null;
		}
	}

}
