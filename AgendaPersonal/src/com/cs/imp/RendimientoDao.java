package com.cs.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cs.models.Rendimiento;
import com.cs.utils.AbsFacade;
import com.cs.utils.Dao;

@Repository
@Transactional
public class RendimientoDao extends AbsFacade<Rendimiento> implements Dao<Rendimiento>{

	@Autowired
	private SessionFactory startSession;
	
	public RendimientoDao(SessionFactory sessionFactory) {
		super(Rendimiento.class);
		this.startSession = sessionFactory;
	}

	@Override
	public SessionFactory sessionFactory() {
		return startSession;		
	}

}
