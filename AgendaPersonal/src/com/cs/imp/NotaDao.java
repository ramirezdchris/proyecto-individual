package com.cs.imp;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cs.models.Evento;
import com.cs.models.Nota;
import com.cs.utils.AbsFacade;
import com.cs.utils.Dao;

@Repository
@Transactional
public class NotaDao extends AbsFacade<Nota> implements Dao<Nota>{

	@Autowired
	private SessionFactory startSession;
	
	public NotaDao(SessionFactory sessionFactory) {
		super(Nota.class);
		this.startSession = sessionFactory;
	}

	@Override
	public SessionFactory sessionFactory() {
		return startSession;		
	}

	public List<Nota> notasEvento(int idEvento) {
		Query q;
		try {
			q = sessionFactory().getCurrentSession().createNativeQuery("SELECT * FROM nota WHERE id_evento = " +idEvento, Nota.class);
			List<Nota> list = q.getResultList();
			return list;			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error en DAO Evento Metodo eventoRealizada" +e.getMessage());
			return null;
		}
		
	}
}