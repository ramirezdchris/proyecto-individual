package com.cs.imp;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cs.models.Asignaciones;
import com.cs.models.AsignacionesId;
import com.cs.models.Usuario;
import com.cs.utils.AbsFacade;
import com.cs.utils.Dao;

@Repository
@Transactional
public class AsignacionesDao extends AbsFacade<Asignaciones> implements Dao<Asignaciones>{

	@Autowired
	private SessionFactory startSession;
	
	public AsignacionesDao(SessionFactory sessionFactory) {
		super(Asignaciones.class);
		this.startSession = sessionFactory;
	}

	@Override
	public SessionFactory sessionFactory() {
		return startSession;		
	}
	
	public List<Asignaciones> listaAsignacionesPendientes(int idUsuario){
		Query q;
		
		List<Asignaciones> listaAsignaciones = new ArrayList<Asignaciones>();
		try {
			q = sessionFactory().getCurrentSession().createNativeQuery("SELECT * FROM asignaciones WHERE id_usuario = "+idUsuario +" AND estado = 'Pendiente';" , Asignaciones.class);
			listaAsignaciones = q.getResultList();
			
		} catch (Exception e) {
			System.out.println("Error en DAO Asignaciones: " +e.getMessage());
			System.out.println("Lista: " +listaAsignaciones);
			listaAsignaciones = null;
		}
		return listaAsignaciones;
		
	}
	
	public List<Asignaciones> listaAsignaciones(int idUsuario){
		Query q;
		
		List<Asignaciones> listaAsignaciones = new ArrayList<Asignaciones>();
		try {
			q = sessionFactory().getCurrentSession().createNativeQuery("SELECT * FROM asignaciones WHERE id_usuario = "+idUsuario +";" , Asignaciones.class);
			listaAsignaciones = q.getResultList();
			
		} catch (Exception e) {
			System.out.println("Error en DAO Asignaciones: " +e.getMessage());
			System.out.println("Lista: " +listaAsignaciones);
			listaAsignaciones = null;
		}
		return listaAsignaciones;
		
	}

	public void asignacionRealizada(int idEvento, int idUsuario) {
		Query q;
		try {
			q = sessionFactory().getCurrentSession().createNativeQuery("UPDATE asignaciones SET estado = 'Realizado' WHERE id_evento = " +idEvento +" AND id_usuario = " +idUsuario +";"); 
			q.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error en DAO Asignaciones Metodo asignacionRealizada" +e.getMessage());
		}
		
	}

	

	/*public List<Usuario> listaEmpleados(){
		Query q;
		List<Usuario> listaEmpleados;
		try {
			q = sessionFactory().getCurrentSession().createNamedQuery("SELECT U FROM Usuario U WHERE U.subo");
		} catch (Exception e) {
				// TODO: handle exception
			}
	}*/
	
}
