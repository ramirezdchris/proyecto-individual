 -- DROP DATABASE AgendaPersonal;
CREATE DATABASE AgendaPersonal;
USE AgendaPersonal;

CREATE TABLE usuario
(
	id_usuario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(30) NOT NULL,
    apellido VARCHAR(30) NOT NULL,
    email VARCHAR(50) NOT NULL,
    pass VARCHAR(50) NOT NULL
);

CREATE TABLE prioridad
(
	id_prioridad INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    prioridad VARCHAR(20) NOT NULL
);

CREATE TABLE evento
(
	id_evento INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    titulo VARCHAR(200) NOT NULL,
    descripcion VARCHAR(2000) NOT NULL,
    fecha_inicio DATE NOT NULL,
    fecha_fin DATE NOT NULL,
    hora_inicio TIME NOT NULL, 
    hora_fin TIME NOT NULL, 
    estado VARCHAR(30) NOT NULL,
	id_prioridad INT NOT NULL,
    id_usuario INT NOT NULL
    
);


CREATE TABLE nota
(
	id_nota INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nota VARCHAR(2000) NOT NULL,    
	id_evento INT NOT NULL
);

CREATE TABLE rendimiento
(
	id_redimiento INT NOT NULL PRIMARY KEY,
    tareasRealizadas INT NOT NULL,
    calculo DOUBLE NOT NULL,
    id_usuario INT NOT NULL
);
	
INSERT INTO prioridad VALUES(0,'Alta'),(0,'Media'),(0,'Baja');

ALTER TABLE evento ADD CONSTRAINT fk_evento_prioridad FOREIGN KEY(id_prioridad) REFERENCES prioridad(id_prioridad);
ALTER TABLE evento ADD CONSTRAINT fk_evento_usuario FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario);

ALTER TABLE nota ADD CONSTRAINT fk_nota_evento FOREIGN KEY(id_evento) REFERENCES evento(id_evento);

ALTER TABLE rendimiento ADD CONSTRAINT fk_rendimiento_usuario FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario);

SELECT * FROM Usuario;
SELECT * FROM evento;

SELECT * FROM prioridad;

SET FOREIGN_KEY_CHECKS = 0;
SET FOREIGN_KEY_CHECKS = 1;
TRUNCATE usuario;

TRUNCATE evento;




