<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@include file="header.jsp"%>
<style type="text/css">
fieldset {width 120px;
	display: block;
	margin-left: 2px;
	margin-right: 2px;
	padding-top: 0.35em;
	padding-bottom: 0.625em;
	padding-left: 0.75em;
	padding-right: 0.75em;
	border: 1px groove #ced4da;
	border-radius: 5px;
}

legend {
	width: 120px;
	margin-left: 20px;
	padding-left: 20px;
}

button:hover {
	background-color: #003366;
	color: white;
}
</style>

</head>
<body>
	<%@include file="navbar.jsp"%>

	<div class="container">
		<div class="row">
			<div class="col">
				<button type="button" class="btn btn-info btn-lg"
					data-toggle="modal" data-target="#myModal">Open Modal</button>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<ul class="main">
					<li class="date">
						<h3>Dec 18</h3>
						<p>Schedule of Events</p>
					</li>
					<li class="events">
						<ul class="events-detail">
							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>

							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>

							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>

							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>

							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>
						</ul>


					</li>



					<li class="date">
						<h3>Dec 19</h3>
						<p>Schedule of Events</p>
					</li>
					<li class="events cf">
						<ul class="events-detail">
							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>

							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>

							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>

							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>

							<li><a href="#"> <span class="event-time">2:00pm
										- </span> <span class="event-name">Kickoff Ceremony</span> <br /> <span
									class="event-location">Headquarters</span>
							</a></li>
						</ul>


					</li>

				</ul>
			</div>
		</div>

	</div>


	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<form action="registroEvento" method="GET">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header"
						style="background-color: #003366; color: white;">
						<h4 class="modal-title">Registro de Tarea</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body" style="background-color:">

						<div class="row">
							<div class="col-sm text-center">
								<label style="font-size: 30px">Titulo</label>
							</div>
							<div class="col-sm text-center">
								<input type="text" name="titulo" class="form-control" />
							</div>
						</div>

						<div class="row">
							<div class="col-sm text-center">
								<label>Descripcion</label>
							</div>
							<div class="col-sm text-center">
								<textarea rows="5" cols="22" name="descripcion"
									class="form-control"></textarea>
							</div>
						</div>


						<fieldset>
							<legend>Fechas</legend>
							<div class="row">


								<div class="col-sm text-center">
									<label>Fecha Inicio</label>
								</div>
								<div class="col-sm text-center">
									<input type="date" name="fechaInicio" class="form-control">
								</div>

							</div>


							<div class="row">
								<div class="col-sm text-center">
									<label>Fecha Fin</label>
								</div>
								<div class="col-sm text-center">
									<input type="date" name="fechaFin" class="form-control">
								</div>
							</div>
						</fieldset>
						<br>

						<fieldset>
							<legend>Horarios</legend>
							<div class="row">
								<div class="col-sm text-center">
									<label>Hora de Inicio</label>
								</div>
								<div class="col-sm text-center">
									<input type="time" name="horaInicio" class="form-control">
								</div>
							</div>

							<div class="row">
								<div class="col-sm text-center">
									<label>Hora de Finalizacion</label>
								</div>
								<div class="col-sm text-center">
									<input type="time" name="horaFin" class="form-control">
								</div>
							</div>
						</fieldset>

						<br>


						<div class="row">
							<div class="col-sm text-center">
								<label>Prioridad de Tarea</label>
							</div>
							<div class="col-sm text-center">
								<select class="form-control" name="prioridad">
									<c:forEach items="${listaPrio}" var="prio">
										<option value="${prio.getIdPrioridad()}">${prio.getPrioridad()}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<br>
						<div class="row">
							<div class="col-md text-center">
								<input type="submit" value="Registrar Tarea"
									class="btn btn-success">
							</div>
						</div>

						${msg}
			</form>


		</div>
		<div class="modal-footer"">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
	</div>






	<%@include file="footer.jsp"%>



</body>
</html>