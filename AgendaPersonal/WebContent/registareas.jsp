<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="validarSesion.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@include file="header.jsp"%>
<style type="text/css">
fieldset {width 120px;
	display: block;
	margin-left: 2px;
	margin-right: 2px;
	padding-top: 0.35em;
	padding-bottom: 0.625em;
	padding-left: 0.75em;
	padding-right: 0.75em;
	border: 1px groove #ced4da;
	border-radius: 5px;
}

legend {
	width: 300px;
	margin-left: 20px;
	padding-left: 20px;
}

button:hover {
	background-color: #003366;
	color: white;
}
</style>

</head>
<body>
	<%@include file="navbar.jsp"%>

	<div class="container">
		<br> <br>
		<div class="row">
			<div class="col">
				<button type="button" class="btn btn-info btn-lg"
					data-toggle="modal" data-target="#myModal">
					Agregar un evento <i class="fa fa-calendar-plus-o "></i>
				</button>
			</div>
		</div>

		<br> <br>

		<div class="row text-center">
			<div class="col">

				<ul class="nav nav-tabs justify-content-center" id="myTab"
					role="tablist" style="border-bottom: none;">
					<li class="nav-item"><a class="nav-link active" id="home-tab"
						data-toggle="tab" href="#home" role="tab" aria-controls="home"
						aria-selected="true"
						style="background-color: #82CC77; color: black;">Mis Tareas</a></li>
					<li class="nav-item"><a class="nav-link" id="profile-tab"
						data-toggle="tab" href="#profile" role="tab"
						aria-controls="profile" aria-selected="false"
						style="background-color: #FF7D7D; color: black;">Tareas
							Asignadas</a></li>
					<li class="nav-item"><a class="nav-link" id="contact-tab"
						data-toggle="tab" href="#contact" role="tab"
						aria-controls="contact" aria-selected="false"
						style="background-color: #E1DEDE; color: black;">Todas las
							tareas</a></li>
				</ul>
			</div>


		</div>



		<div class="tab-content" id="myTabContent"
			style="border-color: black; border-radius: 2px solid #CDFFC8">
			<div class="tab-pane fade show active" id="home" role="tabpanel"
				aria-labelledby="home-tab">
				<div class="row"
					style="border: 2px solid #82CC77; border-radius: 15px">
					<div class="col" style="margin-bottom: 50px;">
						<ul class="main">
							<c:forEach items="${listaEventos}" var="e">
								<li class="date">
									<h3>
										<fmt:formatDate value="${e.getFechaInicio()}" type="date" />
									</h3>
									<p></p>
								</li>
								<li class="events">
									<ul class="events-detail">
										<li><a href="#" onclick=""> <span class="event-time">${e.getHoraInicio()}
													- ${e.getHoraFin()}</span> <span class="event-name">${e.getTitulo()}</span>
												<br /> <span class="event-location">${e.getDescripcion()}</span>
										</a>
											<button class="btn btn-danger"
												onclick="alerta_finalizar_evento('${e.getIdEvento()}', '${e.getTitulo()}')">Finalizar
												Evento</button>&nbsp;&nbsp;
											<button type="button" class="btn btn-info"
												data-toggle="modal" data-target="#myModal2"
												onclick="modal_tareas('${e.getIdEvento()}', '${e.getTitulo()}')">
												Asignar eventos <i class="fa fa-calendar-plus-o "></i>
											</button>&nbsp;&nbsp;
											<button type="button" class="btn btn-info"
												data-toggle="modal" data-target="#myModal3"
												onclick="notas('${e.getIdEvento()}')" style="background-color: #FFC900; color: black; border-color: black;">
												Agregar Nota <i class="fa fa-sticky-note"></i>
											</button>
									</ul>
								</li>
							</c:forEach>
						</ul>
					</div>
					<br> <br>
				</div>
			</div>

			<div class="tab-pane fade" id="profile" role="tabpanel"
				aria-labelledby="profile-tab">
				<div class="row"
					style="border: 2px solid #F59A9A; border-radius: 15px">
					<div class="col" style="margin-bottom: 50px;">
						<ul class="main">
							<c:forEach items="${listaEventosAsignados}" var="e">
								<li class="date">
									<h3>
										<fmt:formatDate value="${e.getEvento().getFechaInicio()}"
											type="date" />
									</h3>
									<p></p>
								</li>
								<li class="events">
									<ul class="events-detail">
										<li><a href="#" onclick=""> <span class="event-time">${e.getEvento().getHoraInicio()}
													- ${e.getEvento().getHoraFin()}</span> <span class="event-name">${e.getEvento().getTitulo()}</span>
												<br /> <span class="event-location">${e.getEvento().getDescripcion()}</span>
										</a>
											<button class="btn btn-danger"
												onclick="alerta_finalizar_tarea('${e.getEvento().getIdEvento()}')">Dar
												por terminada</button>
									</ul>
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</div>


			<div class="tab-pane fade" id="contact" role="tabpanel"
				aria-labelledby="contact-tab">
				<div class="row"
					style="border: 2px solid #E1DEDE; border-radius: 15px">
					<div class="col" style="margin-bottom: 50px;">
					<br>
					<center><h3 style="border-bottom: 2px solid #E1DEDE; padding-bottom: 10px;">Mis Eventos</h3></center>
						<ul class="main">
							<c:forEach items="${listaEventosTodos}" var="e">
								<li class="date">
									<h3>
										<fmt:formatDate value="${e.getFechaInicio()}" type="date" />
									</h3>
									<p></p>
								</li>
								<li class="events">
									<ul class="events-detail">
										<li><a href="#" onclick=""> <span class="event-time">${e.getHoraInicio()}
													- ${e.getHoraFin()}</span> <span class="event-name">${e.getTitulo()}</span>
												<br /> <span class="event-location">${e.getDescripcion()}</span>
										</a>										
									</ul>
								</li>
							</c:forEach>
						</ul>
					</div>
					<div style="border-left: 6px solid #E1DEDE;"></div>
					<div class="col" style="margin-bottom: 50px;">
					<br>
					<center><h3 style="border-bottom: 2px solid #E1DEDE; padding-bottom: 10px;">Eventos Asignados</h3></center>
						<ul class="main">
							<c:forEach items="${listaEventosAsignadosTodos}" var="e">
								<li class="date">
									<h3>
										<fmt:formatDate value="${e.getEvento().getFechaInicio()}"
											type="date" />
									</h3>
									<p></p>
								</li>
								<li class="events">
									<ul class="events-detail">
										<li><a href="#" onclick=""> <span class="event-time">${e.getEvento().getHoraInicio()}
													- ${e.getEvento().getHoraFin()}</span> <span class="event-name">${e.getEvento().getTitulo()}</span>
												<br /> <span class="event-location">${e.getEvento().getDescripcion()}</span>
										</a>
									</ul>
								</li>
							</c:forEach>
						</ul>
					</div>

				</div>
			</div>

		</div>

		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header"
						style="background-color: #003366; color: white;">
						<h4 class="modal-title">Registro de Tarea</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body" style="background-color:">
						<form action="registroEvento" method="GET">
							<div class="row">
								<div class="col-sm text-center">
									<label style="font-size: 30px">Titulo</label>
								</div>
								<div class="col-sm text-center">
									<input type="text" name="titulo" class="form-control" />
								</div>
							</div>

							<div class="row">
								<div class="col-sm text-center">
									<label>Descripcion</label>
								</div>
								<div class="col-sm text-center">
									<textarea rows="5" cols="22" name="descripcion"
										class="form-control"></textarea>
								</div>
							</div>


							<fieldset>
								<legend>Fechas</legend>
								<div class="row">


									<div class="col-sm text-center">
										<label>Fecha Inicio</label>
									</div>
									<div class="col-sm text-center">
										<input type="date" name="fechaInicio" class="form-control"
											id="fechaInicio">
									</div>

								</div>


								<div class="row">
									<div class="col-sm text-center">
										<label>Fecha Fin</label>
									</div>
									<div class="col-sm text-center">
										<input type="date" name="fechaFin" class="form-control"
											id="fechaFin">
									</div>
								</div>
							</fieldset>
							<br>

							<fieldset>
								<legend>Horarios</legend>
								<div class="row">
									<div class="col-sm text-center">
										<label>Hora de Inicio</label>
									</div>
									<div class="col-sm text-center">
										<input type="time" name="horaInicio" class="form-control">
									</div>
								</div>

								<div class="row">
									<div class="col-sm text-center">
										<label>Hora de Finalizacion</label>
									</div>
									<div class="col-sm text-center">
										<input type="time" name="horaFin" class="form-control">
									</div>
								</div>
							</fieldset>

							<br>


							<div class="row">
								<div class="col-sm text-center">
									<label>Prioridad de Tarea</label>
								</div>
								<div class="col-sm text-center">
									<select class="form-control" name="prioridad">
										<c:forEach items="${listaPrio}" var="prio">
											<option value="${prio.getIdPrioridad()}">${prio.getPrioridad()}</option>
										</c:forEach>
									</select>
								</div>
							</div>

							<br>
							<div class="row">
								<div class="col-md text-center">
									<input type="submit" value="Registrar Tarea"
										class="btn btn-success">
								</div>
							</div>

							${msg} <input type="hidden" value="${sessionScope.idUsuario}"
								name="usuario">
						</form>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div id="myModal2" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header"
						style="background-color: #003366; color: white;">
						<h4 class="modal-title">Registro de Tarea</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body" style="background-color:">
						<form action="asignarTareas" method="GET">
							<div class="row">
								<div class="col-sm text-center">
									<label style="font-size: 30px">Tarea</label>
								</div>
								<div class="col-sm text-center">
									<input class="form-control" name="evento" id="eventoId"
										type="hidden" value="" /> <input class="form-control"
										name="eventoNombre" id="eventoNombre" value="" type="text" />
								</div>
							</div>

							<br>
							<fieldset>
								<legend>Empleados</legend>
								<div class="row">
									<div class="col-md text-center">
										<div class="row text-left">
											<c:forEach items="${listaSubor}" var="emp">

												<div class="col-4">
													<input type="checkbox" name="empleados"
														value="${emp.getIdUsuario()}" class="checkbox"> <label>${emp.getNombre()}</label>
												</div>

											</c:forEach>
										</div>
									</div>
								</div>
							</fieldset>

							<br> <br>
							<div class="row">
								<div class="col-md text-center">
									<input type="submit" value="Registrar Tarea"
										class="btn btn-success">
								</div>
							</div>

							${msg} <input type="hidden" value="${sessionScope.idUsuario}"
								name="usuario">
						</form>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>




		<%@include file="footer.jsp"%>

		<script>
            function alerta_finalizar_evento(cod, nombreEvento) {
                Swal.fire({
                    title: 'Deseas terminar el evento ' +nombreEvento,
                    text: 'Esta accion no se podra deshacer',
                    icon: 'warning',
                    cancelButtonText: "Cancelar",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, Finalizar'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = "eventoEstado?idEvento=" +cod;
                    }
                })
            }

            function alerta_finalizar_tarea(cod) {
                Swal.fire({
                    title: 'Esta seguro que desea terminar la tarea asignada',
                    text: 'Esta accion no se podra deshacer',
                    icon: 'warning',
                    cancelButtonText: "Cancelar",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, Eliminar'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = "asignacionesEstado?idEvento=" + cod;
                    }
                })
            }

            function modal_tareas(idEvento, nombreEvento){
					document.getElementById("eventoId").value = idEvento;
					document.getElementById("eventoNombre").value = nombreEvento;
					document.getElementById("eventoNombre").disabled = true;
            }


            // Funcionar para no escoger fechas anteriores a la actual
            window.onload = function() {
//            	document.getElementById('fechaInicio').value = new Date().toDateInputValue();
            	var fecha = new Date();
                var anio = fecha.getFullYear();
                var dia = fecha.getDate();
                var _mes = fecha.getMonth();//viene con valores de 0 al 11
                console.log(_mes);
                _mes = _mes + 1;//ahora lo tienes de 1 al 12
                console.log(_mes);
                if (_mes < 10)//ahora le agregas un 0 para el formato date
                { var mes = "0" + _mes;}
                else
                { var mes = _mes.toString;}
                document.getElementById("fechaInicio").min = anio+'-'+mes+'-'+dia; 
                document.getElementById("fechaFin").min = anio+'-'+mes+'-'+dia;
                
            };


            function notas(evento){
				window.location.href = "notas?evento=" + evento;
            }
        </script>
</body>
</html>