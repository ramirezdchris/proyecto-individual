<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<%@include file="header.jsp"%>

<style type="text/css">
@import url(import url here);

body {
	/*font: normal 1em 'font';*/
	background-color: #000000;
}

main {
/*	margin-top: 4.5em; */
}
#section1 {
	height: 30em;
}


/* Multi-Step Form */
* {
  box-sizing: border-box;
}

#regForm {
  background-color: #fff;
  margin: 100px auto;
  font-family: Raleway;
  padding: 40px;
  width: 100%;
  min-width: 600px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that get errors during validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Step marker: Place in the form. */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}

#box {
  display: flex;
  align-items: center;
  justify-content: center;
  width: 400px;
  height: 200px;
  color: white;
  font-family: 'Raleway';
  font-size: 2.5rem;
}
.gradient-border {
  --borderWidth: 3px;
  background: #1D1F20;
  position: relative;
  border-radius: var(--borderWidth);
}
.gradient-border:after {
  content: '';
  position: absolute;
  top: calc(-1 * var(--borderWidth));
  left: calc(-1 * var(--borderWidth));
  height: calc(100% + var(--borderWidth) * 2);
  width: calc(100% + var(--borderWidth) * 2);
  background: linear-gradient(60deg, #f79533, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82);
  border-radius: calc(2 * var(--borderWidth));
  z-index: -1;
  animation: animatedgradient 3s ease alternate infinite;
  background-size: 300% 300%;
}


@keyframes animatedgradient {
	0% {
		background-position: 0% 50%;
	}
	50% {
		background-position: 100% 50%;
	}
	100% {
		background-position: 0% 50%;
	}
}
</style>
</head>
<body>


	<h1 class="bg-danger text-white text-center p-2 fixed-top">Bievenido a Tu Agenda</h1>

	<main class="content" role="content">

	<section id="section1">
		<div class="container-fluid col-md-6 col-md-offset-3">

			<!-- MultiStep Form -->
			<form id="regForm" action="registroUsuario" method="GET">
				<h1>Registro</h1>
				<!-- One "tab" for each step in the form: -->
				<div class="tab">
					Nombre Completo
					<p>
						<input placeholder="Nombres" oninput="this.className = ''"
							name="nombre">
					</p>
					<p>
						<input placeholder="Apellidos" oninput="this.className = ''"
							name="apellido">
					</p>
				</div>
				<div class="tab">
					Credenciales
					<p>
						<input placeholder="Correo." oninput="this.className = ''"
							name="email">
					</p>
					<p>
						<input placeholder="Contraseņa" oninput="this.className = ''"
							name="pass" type="password">
					</p>
				</div>							
				<div class="tab">
				
				</div>			
								
				
				<div style="overflow: auto;">
					<div style="float: right;">
						<button type="button" id="prevBtn" onclick="nextPrev(-1)">Volver</button>
						<button type="button" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
					</div>
				</div>
				<!-- Circles which indicates the steps of the form: -->
				
				<div style="text-align: center; margin-top: 40px;">
					<span class="step"></span> <span class="step"></span>
					<span class="step"></span> <span class="step"></span>  
				</div>
			</form>
			
			<h1>${msg}</h1>

		</div>
	</section>

	</main>
	
	<%@ include file="footer.jsp" %>

	<script type="text/javascript">
	$(document).ready(function() {	
		
		// Random Alert shown for the fun of it
		function randomAlert() {
			var min = 5,
				max = 20;
			var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 5 - 20
			// post time in a <span> tag in the Alert
			$("#time").html('Next alert in ' + rand + ' seconds');
			$('#timed-alert').fadeIn(500).delay(3000).fadeOut(500);
			setTimeout(randomAlert, rand * 1000);
		};
		randomAlert();
	});

	$('.btn').click(function(event) {
	    event.preventDefault();
	    var target = $(this).data('target');
		// console.log('#'+target);
		$('#click-alert').html('data-target= ' + target).fadeIn(50).delay(3000).fadeOut(1000);
		
	});


	// Multi-Step Form
	var currentTab = 0; // Current tab is set to be the first tab (0)
	showTab(currentTab); // Display the crurrent tab

	function showTab(n) {
	  // This function will display the specified tab of the form...
	  var x = document.getElementsByClassName("tab");
	  x[n].style.display = "block";
	  //... and fix the Previous/Next buttons:
	  if (n == 0) {
	    document.getElementById("prevBtn").style.display = "none";
	  } else {
	    document.getElementById("prevBtn").style.display = "inline";
	  }
	  if (n == (x.length - 1)) {
	    document.getElementById("nextBtn").innerHTML = "Registrarse";
	  } else {
	    document.getElementById("nextBtn").innerHTML = "Siguiente";
	  }
	  //... and run a function that will display the correct step indicator:
	  fixStepIndicator(n)
	}

	function nextPrev(n) {
	  // This function will figure out which tab to display
	  var x = document.getElementsByClassName("tab");
	  // Exit the function if any field in the current tab is invalid:
	  if (n == 1 && !validateForm()) return false;
	  // Hide the current tab:
	  x[currentTab].style.display = "none";
	  // Increase or decrease the current tab by 1:
	  currentTab = currentTab + n;
	  // if you have reached the end of the form...
	  if (currentTab >= x.length) {
	    // ... the form gets submitted:
	    document.getElementById("regForm").submit();
	    return false;
	  }
	  // Otherwise, display the correct tab:
	  showTab(currentTab);
	}

	function validateForm() {
	  // This function deals with validation of the form fields
	  var x, y, i, valid = true;
	  x = document.getElementsByClassName("tab");
	  y = x[currentTab].getElementsByTagName("input");
	  // A loop that checks every input field in the current tab:
	  for (i = 0; i < y.length; i++) {
	    // If a field is empty...
	    if (y[i].value == "") {
	      // add an "invalid" class to the field:
	      y[i].className += " invalid";
	      // and set the current valid status to false
	      valid = false;
	    }
	  }
	  // If the valid status is true, mark the step as finished and valid:
	  if (valid) {
	    document.getElementsByClassName("step")[currentTab].className += " finish";
	  }
	  return valid; // return the valid status
	}

	function fixStepIndicator(n) {
	  // This function removes the "active" class of all steps...
	  var i, x = document.getElementsByClassName("step");
	  for (i = 0; i < x.length; i++) {
	    x[i].className = x[i].className.replace(" active", "");
	  }
	  //... and adds the "active" class on the current step:
	  x[n].className += " active";
	}
	</script>



</body>
</html>