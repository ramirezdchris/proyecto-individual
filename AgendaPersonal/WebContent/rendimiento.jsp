<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="validarSesion.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@include file="header.jsp"%>
<style type="text/css">
fieldset {width 120px;
	display: block;
	margin-left: 2px;
	margin-right: 2px;
	padding-top: 0.35em;
	padding-bottom: 0.625em;
	padding-left: 0.75em;
	padding-right: 0.75em;
	border: 1px groove #ced4da;
	border-radius: 5px;
}

legend {
	width: 300px;
	margin-left: 20px;
	padding-left: 20px;
}

button:hover {
	background-color: #003366;
	color: white;
}
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
	<%@include file="navbar.jsp"%>

	<div class="container">
		<br> <br>
		<div class="row">
			<div class="col">
				<div id="chart_div"></div>
			</div>
		</div>

	

		<br> <br>

		

	</div>


	<%@include file="footer.jsp"%>

	<script>
           
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                  /*var data = google.visualization.arrayToDataTable([
                      ['Notas','Notas','Notas'],
                	  <c:forEach items="${listaNotas}" var="entry">
                      [ '${entry.getNota()}',1,1],
                  	</c:forEach>
                  ]);
*/

                  var data = google.visualization.arrayToDataTable([
                      ['Tareas','Tareas Pendientes','Tareas Realizadas'],
                      ['Tareas Realizadas', ${tareaRealizada},0],            // RGB value
                      ['Tareas Pendientes',0,${tareaPendiente}]
                   ]);
                  
                  var options = {
                    title: 'Rendimiento',
                    chartArea: {width: '50%'},
                    hAxis: {
                      title: 'Total Tareas ${totalEvento}',
                      minValue: 0
                    },
                    vAxis: {
                      title: 'Tareas'
                    },
                    width: "100%",
                    height: "600",
                    colors:['#82CC77','#FF7D7D']
                  };

                  var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
                  chart.draw(data, options);
                }

            /*
            function drawChart() {
                // Create the data table.
                var data = google.visualization.arrayToDataTable([
                                                                      ['Country', 'Area(square km)'],
                                                                      <c:forEach items="${pieDataList}" var="entry">
                                                                          [ '${entry.key}', ${entry.value} ],
                                                                      </c:forEach>
                                                                ]);
                // Set chart options
                var options = {
                    'title' : 'Area-wise Top Seven Countries in the World',
                    is3D : true,
                    pieSliceText: 'label',
                    tooltip :  {showColorCode: true},
                    'width' : 900,
                    'height' : 500
                };
                // Instantiate and draw our chart, passing in some options.
                var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            }*/
        </script>

</body>
</html>