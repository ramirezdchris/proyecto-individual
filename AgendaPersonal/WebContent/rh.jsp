<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@include file="header.jsp"%>
<style type="text/css">
fieldset {width 120px;
	display: block;
	margin-left: 2px;
	margin-right: 2px;
	padding-top: 0.35em;
	padding-bottom: 0.625em;
	padding-left: 0.75em;
	padding-right: 0.75em;
	border: 1px groove #ced4da;
	border-radius: 5px;
}

legend {
	width: 120px;
	margin-left: 20px;
	padding-left: 20px;
}

button:hover {
	background-color: #003366;
	color: white;
}
</style>

</head>
<body>
	<%@include file="navbar.jsp"%>

	<div class="container">
	<br><br>
		<div class="row">
			
			<div class="col"><h1>Asignacion de jefes a nuevos usuarios</h1></div>
		</div>
		<br> <br>
		<div class="row">
			<div class="col-10">
				<table id="table-1" class="table table-hover">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Correo</th>
							<th>Tipo de Usuario</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listEmpSin}" var="e">
							<tr>
								<td>${e.getNombre()}</td>
								<td>${e.getEmail()}</td>
								<td>${e.getTipousuario().getTipo()}</td>
								<td><button type="button" class="btn btn-info btn-lg"
										data-toggle="modal" data-target="#myModal"
										onclick="editar('${e.getIdUsuario()}','${e.getNombre()}','${e.getTipousuario().getTipo()}')">Editar</button>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<form action="actualizar" method="GET">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header"
						style="background-color: #003366; color: white;">
						<h4 class="modal-title">Actualizar Empleados</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body" style="background-color:">

						<input type="hidden" id="idSubor" name="idSubor" /> <br>

						<fieldset>
							<legend>Jefe</legend>
							<div class="row">
								<div class="col-sm text-center">
									<label>Nombre</label>
								</div>
								<div class="col-sm text-center">
									<select name="idJefe" class="form-control">
										<option disabled selected value>Seleccione un Jefe</option>
										<c:forEach items="${listEmpJefe}" var="jefe">
											<option value="${jefe.getIdUsuario()}">${jefe.getNombre()}</option>
										</c:forEach>
									</select>
								</div>
							</div>

						</fieldset>

						<br>

						<fieldset>
							<legend>Subordinado</legend>
							<div class="row">
								<div class="col-sm text-center">
									<label>Subordinado</label>
								</div>
								<div class="col-sm text-center">
									<input type="text" id="nombreSubor" name="nombreSubor"
										class="form-control">
								</div>
							</div>

							<div class="row" style="margin-top: 5px;">
								<div class="col-sm text-center">
									<label>Cargo del Subordinado</label>
								</div>
								<div class="col-sm text-center">

									<select name="cargoSubor" class="form-control">
										<option disabled selected value>Rol</option>
										<option value="3">Supervisor</option>
										<option value="4">Colaborador</option>
									</select>
								</div>
							</div>
						</fieldset>
						<br>
						<div class="row">
							<div class="col-md text-center">
								<input type="submit" value="Actualizar"
									class="btn btn-success">
							</div>
						</div>

						${msg} <input type="hidden" value="${sessionScope.idUsuario}"
							name="usuario">
			</form>


		</div>
		<div class="modal-footer"">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
	</div>

	<%@include file="footer.jsp"%>

	<script>
		function editar(id, nombre, tipousu) {
			document.getElementById("idSubor").value = id;
			document.getElementById("nombreSubor").value = nombre;
			document.getElementById("cargoSubor").value = tipousu;
		}
	</script>

</body>
</html>