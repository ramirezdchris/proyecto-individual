<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
h2{
color: rgba(255,255,255,.5);
}
a{
color: white;
}
</style>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	<a class="navbar-brand" href="menu.jsp"><i class="fa fa-calendar"></i></a>
	<!-- Links -->
	<h2>||</h2>
	<ul class="navbar-nav mr-auto">
		<li class="nav-item"><a class="nav-link" href="eventos" style="color: white;">Registro de Eventos&nbsp&nbsp<i class="fa fa-calendar-check-o"></i></a></li>
		<h2>||</h2>		
		<li class="nav-item"><a class="nav-link" href="rendimiento" style="color: white;">Rendimiento&nbsp&nbsp<i class="fa fa-area-chart "></i></a></li>
		<c:if test="${sessionScope.rol == 2}">
		<h2>||</h2>
		<li class="nav-item"><a class="nav-link" href="listasEmpleados" style="color: white;">Asignar Empleados&nbsp&nbsp<i class="fa fa-area-chart "></i></a></li>
		</c:if>
		<h2>||</h2>
		<!--
		<c:if test="${sessionScope.rol == 1}">
		<h2>||</h2>
		<li class="nav-item"><a class="nav-link" href="eventosUsuario" style="color: white;">Asignar tareas&nbsp&nbsp<i class="fa fa-list-ol"></i></a></li>
		</c:if>
		
		<c:if test="${sessionScope.rol == 3}">
		<h2>||</h2>
		<li class="nav-item"><a class="nav-link" href="eventosUsuario" style="color: white;">Asignar tareas&nbsp&nbsp<i class="fa fa-area-chart "></i></a></li>
		</c:if>
		-->
	</ul>
	
	<h4 style="color: white;">${sessionScope.user}</h4>
	&nbsp&nbsp
	<a href="salir" class="btn btn-danger">Salir</a>	
</nav>
