<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="validarSesion.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@include file="header.jsp"%>
<style type="text/css">
fieldset {width 120px;
	display: block;
	margin-left: 2px;
	margin-right: 2px;
	padding-top: 0.35em;
	padding-bottom: 0.625em;
	padding-left: 0.75em;
	padding-right: 0.75em;
	border: 1px groove #ced4da;
	border-radius: 5px;
}

legend {
	width: 120px;
	margin-left: 20px;
	padding-left: 20px;
}

button:hover {
	background-color: #003366;
	color: white;
}
</style>

</head>
<body>
	<%@include file="navbar.jsp"%>

	<div class="container">
		<br> <br>
		<div class="row">
			<div class="col">
				<button type="button" class="btn btn-info btn-lg"
					data-toggle="modal" data-target="#myModal">
					Asignar eventos <i class="fa fa-calendar-plus-o "></i>
				</button>
			</div>
		</div>



		<div class="row">
			<div class="col">
				<ul class="main">
					<c:forEach items="${listaEventosAsignados}" var="e">
						<li class="date">
							<h3>
								<fmt:formatDate value="${e.getEvento().getFechaInicio()}" type="date" />
							</h3>
							<p></p>
						</li>
						<li class="events">
							<ul class="events-detail">
								<li><a href="#" onclick=""> <span class="event-time">${e.getEvento().getHoraInicio()}
											- ${e.getEvento().getHoraFin()}</span> <span class="event-name">${e.getEvento().getTitulo()}</span>
										<br /> <span class="event-location">${e.getEvento().getDescripcion()}</span>
								</a></li>
							</ul>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>

	</div>


	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<form action="asignarTareas" method="GET">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header"
						style="background-color: #003366; color: white;">
						<h4 class="modal-title">Registro de Tarea</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body" style="background-color:">

						<div class="row">
							<div class="col-sm text-center">
								<label style="font-size: 30px">Tarea</label>
							</div>
							<div class="col-sm text-center">
								<select name="evento" class="form-control">
									<c:forEach items="${listaEventos}" var="evento">
										<option value="${evento.getIdEvento()}">${evento.getTitulo()}</option>
									</c:forEach>
								</select>

							</div>
						</div>

						<br>
						<fieldset>
							<legend>Empleados</legend>
							<div class="row">
								<div class="col-md text-center">
									<div class="row text-left">
										<c:forEach items="${listaSubor}" var="emp">

											<div class="col-4">
												<input type="checkbox" name="empleados"
													value="${emp.getIdUsuario()}" class="checkbox"> <label>${emp.getNombre()}</label>
											</div>

										</c:forEach>
									</div>
								</div>
							</div>
						</fieldset>

						<br> <br>
						<div class="row">
							<div class="col-md text-center">
								<input type="submit" value="Registrar Tarea"
									class="btn btn-success">
							</div>
						</div>

						${msg} <input type="hidden" value="${sessionScope.idUsuario}"
							name="usuario">
			</form>


		</div>
		<div class="modal-footer"">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
	</div>






	<%@include file="footer.jsp"%>

	<script type="text/javascript">
		swal({
			title : "Are you sure?",
			text : "You will not be able to recover this imaginary file!",
			type : "warning",
			showCancelButton : true,
			confirmButtonClass : "btn-danger",
			confirmButtonText : "Yes, delete it!",
			cancelButtonText : "No, cancel plx!",
			closeOnConfirm : false,
			closeOnCancel : false
		}, function(isConfirm) {
			if (isConfirm) {
				swal("Deleted!", "Your imaginary file has been deleted.",
						"success");
			} else {
				swal("Cancelled", "Your imaginary file is safe :)", "error");
			}
		});
	</script>

</body>
</html>