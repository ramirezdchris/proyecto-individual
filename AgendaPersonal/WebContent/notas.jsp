<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="validarSesion.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@include file="header.jsp"%>
<style type="text/css">
fieldset {width 120px;
	display: block;
	margin-left: 2px;
	margin-right: 2px;
	padding-top: 0.35em;
	padding-bottom: 0.625em;
	padding-left: 0.75em;
	padding-right: 0.75em;
	border: 1px groove #ced4da;
	border-radius: 5px;
}

legend {
	width: 300px;
	margin-left: 20px;
	padding-left: 20px;
}

button:hover {
	background-color: #003366;
	color: white;
}
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
	<%@include file="navbar.jsp"%>

	<div class="container">
		<br> <br>
		<div class="row">
			<div class="col">
				<button type="button" class="btn btn-info btn-lg"
					data-toggle="modal" data-target="#myModal3"
					onclick="modal_tareas('${e.getIdEvento()}', '${e.getTitulo()}')"
					style="background-color: #FFC900; color: black; border-color: black;">
					Agregar Nota <i class="fa fa-sticky-note"></i>
				</button>
			</div>
		</div>
		
		<br><br>
		<table class="table table-hover">
		<thead><th style="text-align: center;">NOTAS DE EVENTO</th></thead>
			<c:forEach items="${listaNotas}" var="emp">
			<tbody>
				<tr>
					<td>${emp.getNota()}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		<br> <br>

		<!-- <div id="chart_div"></div> -->

	</div>



	<div id="myModal3" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header"
					style="background-color: #FFC900; color: black;">
					<h4 class="modal-title">Notas</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body" style="background-color:">
					<form action="agregarNota" method="GET">
						<div class="row">
							<div class="col-sm text-center">
								<label style="font-size: 30px">Tarea</label>
							</div>
							<div class="col-sm text-center">
								<input class="form-control" name="evento" id="eventoId"
									type="hidden" value="${idEvento}" /> <input
									class="form-control" name="eventoNombre" id="eventoNombre"
									value="${nombreEvento}" type="text" readonly="readonly" />
							</div>
						</div>

						<br>
						<fieldset>
							<legend>Notas de este Evento</legend>
							<div class="row">
								<div class="col-md text-center">
									<div class="row text-left">
										<div class="col-sm text-center">
											<textarea rows="5" cols="22" name="nota" class="form-control"></textarea>
										</div>
									</div>
								</div>
							</div>
						</fieldset>

						<br> <br>
						<div class="row">
							<div class="col-md text-center">
								<input type="submit" value="Agregar Nota"
									class="btn btn-success">
							</div>
						</div>

						${msg} <input type="hidden" value="${sessionScope.idUsuario}"
							name="usuario">
					</form>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>


	<%@include file="footer.jsp"%>

	<script>
            function alerta_finalizar_evento(cod, nombreEvento) {
                Swal.fire({
                    title: 'Deseas terminar el evento ' +nombreEvento,
                    text: 'Esta accion no se podra deshacer',
                    icon: 'warning',
                    cancelButtonText: "Cancelar",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, Finalizar'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = "eventoEstado?idEvento=" +cod;
                    }
                })
            }

            function alerta_finalizar_tarea(cod) {
                Swal.fire({
                    title: 'Esta seguro que desea terminar la tarea asignada',
                    text: 'Esta accion no se podra deshacer',
                    icon: 'warning',
                    cancelButtonText: "Cancelar",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, Eliminar'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = "asignacionesEstado?idEvento=" + cod;
                    }
                })
            }

            function modal_tareas(idEvento, nombreEvento){
					//document.getElementById("eventoId").value = idEvento;
					//document.getElementById("eventoNombre").value = nombreEvento;
					//document.getElementById("eventoNombre").readOnly = true;
            }


            // Funcionar para no escoger fechas anteriores a la actual
            window.onload = function() {
//            	document.getElementById('fechaInicio').value = new Date().toDateInputValue();
            	var fecha = new Date();
                var anio = fecha.getFullYear();
                var dia = fecha.getDate();
                var _mes = fecha.getMonth();//viene con valores de 0 al 11
                console.log(_mes);
                _mes = _mes + 1;//ahora lo tienes de 1 al 12
                console.log(_mes);
                if (_mes < 10)//ahora le agregas un 0 para el formato date
                { var mes = "0" + _mes;}
                else
                { var mes = _mes.toString;}
                document.getElementById("fechaInicio").min = anio+'-'+mes+'-'+dia; 
                document.getElementById("fechaFin").min = anio+'-'+mes+'-'+dia;
                
            };



            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawMultSeries);

            function drawMultSeries() {
                  var data = google.visualization.arrayToDataTable([
                      ['Notas','Notas','Notas'],
                	  <c:forEach items="${listaNotas}" var="entry">
                      [ '${entry.getNota()}',1,1],
                  	</c:forEach>
                  ]);

                  var options = {
                    title: 'Population of Largest U.S. Cities',
                    chartArea: {width: '50%'},
                    hAxis: {
                      title: 'Total Population',
                      minValue: 0
                    },
                    vAxis: {
                      title: 'City'
                    }
                  };

                  var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
                  chart.draw(data, options);
                }

            /*
            function drawChart() {
                // Create the data table.
                var data = google.visualization.arrayToDataTable([
                                                                      ['Country', 'Area(square km)'],
                                                                      <c:forEach items="${pieDataList}" var="entry">
                                                                          [ '${entry.key}', ${entry.value} ],
                                                                      </c:forEach>
                                                                ]);
                // Set chart options
                var options = {
                    'title' : 'Area-wise Top Seven Countries in the World',
                    is3D : true,
                    pieSliceText: 'label',
                    tooltip :  {showColorCode: true},
                    'width' : 900,
                    'height' : 500
                };
                // Instantiate and draw our chart, passing in some options.
                var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            }*/
        </script>

</body>
</html>